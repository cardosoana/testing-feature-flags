defmodule ElixirTestTest do
  use ExUnit.Case
  doctest ElixirTest

  test "read feature flag file" do
    expected_json = Poison.decode!(~s(
      {
        "some_feature_flag": {
          "status": true,
          "description": "flag for some feature"
        }
      }
    ))
    assert ElixirTest.read_feature_flag() == expected_json

  end
end
