defmodule ElixirTest do
  def read_feature_flag do
    response = HTTPoison.get! "https://gitlab.com/cardosoana/testing-feature-flags/raw/master/feature_flags.json"
    Poison.decode!(response.body)
  end
end
